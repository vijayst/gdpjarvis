//
//  GdpDbHelper.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 25/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "GdpDbHelper.h"
#import "Region.h"
#import "Country.h"
#import "Gdp.h"

@implementation GdpDbHelper

- (NSMutableArray*)getRegions
{
    [self openDb];
    
    NSMutableArray *regions = [[NSMutableArray alloc] init];
    NSString *querystring= [NSString stringWithFormat:@"SELECT id, name FROM region;"];
    const char *sql = [querystring UTF8String];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL)!=SQLITE_OK){
        
        NSLog(@"sql problem occured with: %s", sql);
        NSLog(@"%s", sqlite3_errmsg(database));
        
    }
    else
    {
        // you could handle multiple rows here
       
        while (sqlite3_step(statement) == SQLITE_ROW) {
            Region *region = [Region alloc];
            region.id = sqlite3_column_int(statement, 0);
            region.name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            NSLog(@"%d - %@", region.id, region.name);
            [regions addObject:region];
        } // while        
        
    }
    sqlite3_finalize(statement);
    
    return regions;
}


- (NSMutableArray*)getCountries:(int)region
{
    [self openDb];
    
    NSMutableArray *countries = [[NSMutableArray alloc] init];
    NSString *querystring= [NSString stringWithFormat:@"SELECT id, name FROM country where region=%d;", region];
    const char *sql = [querystring UTF8String];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL)!=SQLITE_OK){
        
        NSLog(@"sql problem occured with: %s", sql);
        NSLog(@"%s", sqlite3_errmsg(database));
        
    }
    else
    {
        // you could handle multiple rows here
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            Country *country = [Country alloc];
            country.shortName = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            country.name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            NSLog(@"%d - %@", country.id, country.name);
            [countries addObject:country];
        }
        
    }
    sqlite3_finalize(statement);
    
    return countries;
}


- (NSMutableArray*)getGdp:(NSString *)country
{
    [self openDb];
    
    NSMutableArray *gdpArray = [[NSMutableArray alloc] init];
    NSString *querystring= [NSString stringWithFormat:@"SELECT country, year, growth, deflator, inflation, nominal_gdp FROM gdp where country='%@' order by year;", country];
    const char *sql = [querystring UTF8String];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL)!=SQLITE_OK){
        
        NSLog(@"sql problem occured with: %s", sql);
        NSLog(@"%s", sqlite3_errmsg(database));
        
    }
    else
    {
        // you could handle multiple rows here
        bool gotFirstValue = false;
        double previousGdpValue = 0;
        while (sqlite3_step(statement) == SQLITE_ROW) {
            Gdp *gdp = [Gdp alloc];
            gdp.countryId = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            gdp.year = sqlite3_column_int(statement, 1);
            gdp.growth = sqlite3_column_double(statement, 2);
            gdp.deflator = sqlite3_column_double(statement, 3);
            gdp.inflation = sqlite3_column_double(statement, 4);
            gdp.nominalGdp = sqlite3_column_double(statement, 5);
            gdp.adjustedGrowthExternal = ((100+gdp.growth)/(100+gdp.deflator)-1)*100;
            gdp.adjustedGrowthInternal = ((100+gdp.growth)/(100+gdp.inflation)-1)*100;
            if(previousGdpValue>0 && gdp.nominalGdp>0) {
                gdp.nominalGrowth = (gdp.nominalGdp-previousGdpValue)/previousGdpValue*100;
            }
            previousGdpValue = gdp.nominalGdp;
                
            NSLog(@"%@ - %d", gdp.countryId , gdp.year);
            if(!gotFirstValue && gdp.growth!=0)
                gotFirstValue=true;
            if(gotFirstValue)
                [gdpArray addObject:gdp];
        }
        
    }
    sqlite3_finalize(statement);
    
    return gdpArray;
}


- (void)openDb
{
    NSString* databasePath = [[NSBundle mainBundle] pathForResource:@"gdp" ofType:@"sqlite"];
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        NSLog(@"Opened sqlite database at %@", databasePath);
    } else {
        NSLog(@"Failed to open database at %@ with error %s", databasePath, sqlite3_errmsg(database));
        sqlite3_close (database);
    }
}

@end


