//
//  DrawText.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 07/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrawText : NSObject
@property NSString *text;
@property CGPoint location;
-(DrawText *)init:(NSString *)text location:(CGPoint)location;
@end
