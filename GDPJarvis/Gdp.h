//
//  Gdp.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 07/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gdp : NSObject
@property NSString *countryId;
@property int year;
@property double growth;
@property double deflator;
@property double inflation;
@property double adjustedGrowthExternal;
@property double adjustedGrowthInternal;
@property double nominalGrowth;
@property double nominalGdp;
@end
