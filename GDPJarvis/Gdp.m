//
//  Gdp.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 07/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "Gdp.h"

@implementation Gdp
@synthesize countryId;
@synthesize year;
@synthesize growth;
@synthesize deflator;
@synthesize inflation;
@synthesize adjustedGrowthExternal;
@synthesize adjustedGrowthInternal;
@synthesize nominalGdp;
@synthesize nominalGrowth;
@end
