//
//  ChartView.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 06/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "ChartView.h"
#import "Chart.h"
#import "GdpDbHelper.h"
#import "Gdp.h"
#import "Series.h"

@implementation ChartView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithFrameAndData:(CGRect)frame data:(NSMutableArray *)data
{
    self = [super initWithFrame:frame];
    if(self)
    {
        gdpArray = data;
        chart = [[Chart alloc] init];
    }
    return self;
}

-(void)setData:(NSMutableArray *)data
{
    gdpArray = data;
}

-(void)drawRect:(CGRect)rect
{
    NSMutableArray *seriesArray = [[NSMutableArray alloc] init];
    
    Series *yearSeries = [Series alloc];
    yearSeries.name = @"Year";
    yearSeries.position = 0;
    yearSeries.values =  [[NSMutableArray alloc] init];
    
    Series *nominalSeries = [Series alloc];
    nominalSeries.name = @"Nominal GDP Growth";
    nominalSeries.position = 1;
    nominalSeries.color = [UIColor greenColor];
    nominalSeries.values = [[NSMutableArray alloc] init];
    nominalSeries.showValue = true;
    
    
    Series *inflationSeries = [Series alloc];
    inflationSeries.name = @"Inflation";
    inflationSeries.position = 2;
    inflationSeries.color = [UIColor redColor];
    inflationSeries.values = [[NSMutableArray alloc] init];
    inflationSeries.showValue = true;
    
    Series *deflatorSeries = [Series alloc];
    deflatorSeries.name = @"GDP Deflator";
    deflatorSeries.position = 3;
    deflatorSeries.color = [UIColor orangeColor];
    deflatorSeries.values = [[NSMutableArray alloc] init];
    deflatorSeries.showValue = true;
    
    Series *growthSeries = [Series alloc];
    growthSeries.name = @"Real GDP Growth";
    growthSeries.position = 4;
    growthSeries.color = [UIColor blueColor];
    growthSeries.values = [[NSMutableArray alloc] init];
    growthSeries.showValue = true;
    
    /*
    Series *growthExternalSeries = [Series alloc];
    growthExternalSeries.name = @"Currency Adjusted GDP Growth";
    growthExternalSeries.position = 4;
    growthExternalSeries.color = [UIColor orangeColor];
    growthExternalSeries.values = [[NSMutableArray alloc] init];
    growthExternalSeries.isLine = true;
    
    Series *growthInternalSeries = [Series alloc];
    growthInternalSeries.name = @"Inflation Adjusted GDP Growth";
    growthInternalSeries.position = 5;
    growthInternalSeries.color = [UIColor redColor];
    growthInternalSeries.values = [[NSMutableArray alloc] init];
    growthInternalSeries.isLine = true;
    growthInternalSeries.showValue = true;
    */
    
    for (Gdp *gdp in gdpArray) {
        [yearSeries.values addObject:[NSNumber numberWithInt:gdp.year]];
        [nominalSeries.values addObject:[NSNumber numberWithDouble:gdp.nominalGrowth]];
        [deflatorSeries.values addObject:[NSNumber numberWithDouble:gdp.deflator]];
        [growthSeries.values addObject:[NSNumber numberWithDouble:gdp.growth]];
        [inflationSeries.values addObject:[NSNumber numberWithDouble:gdp.inflation]];
        /*
         [growthExternalSeries.values addObject:[NSNumber numberWithDouble:gdp.adjustedGrowthExternal]];
        [growthInternalSeries.values addObject:[NSNumber numberWithDouble:gdp.adjustedGrowthInternal]];
         */
    }
    [seriesArray addObject:yearSeries];
    [seriesArray addObject:nominalSeries];
    [seriesArray addObject:inflationSeries];
    [seriesArray addObject:deflatorSeries];
    [seriesArray addObject:growthSeries];

    /*
    [seriesArray addObject:growthExternalSeries];
    [seriesArray addObject:growthInternalSeries];
     */
    [chart setSeries:seriesArray];
    [chart setRect:rect];
    [chart draw];
}
@end
