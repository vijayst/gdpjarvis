//
//  Chart.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "Chart.h"
#import "DrawText.h"

@implementation Chart

@synthesize xMin;
@synthesize xMax;
@synthesize yMin;
@synthesize yMax;
@synthesize width;
@synthesize height;
@synthesize xMargin;
@synthesize yMargin;
@synthesize xSpacing;
@synthesize ySpacing;
@synthesize foreColor;
@synthesize backColor;
@synthesize axisWidth;
@synthesize scaleWidth;
@synthesize majorScaleHeight;
@synthesize minorScaleHeight;
@synthesize xScaleRatio;
@synthesize yScaleRatio;
@synthesize xOrigin;
@synthesize yOrigin;
@synthesize seriesCount;
@synthesize barWidth;
@synthesize xSeries;
@synthesize legend;


-(id)init
{
    self = [super init];
    width = 500;
    height = 500;
    xMin = -1;
    xMax = 10;
    yMin = -10;
    yMax = 20;
    xMargin = yMargin = 20;
    foreColor = [UIColor blackColor];
    backColor = [UIColor colorWithRed:(256.0/256) green:(256.0/256) blue:(204.0/256) alpha:1];
    axisWidth = 2;
    scaleWidth = 1;
    majorScaleHeight = 10;
    minorScaleHeight = 5;
    xScaleRatio = 1;
    yScaleRatio = 2;
    seriesArray = [[NSMutableArray alloc] init];
    legend = [[Legend alloc] init];
    [self setCalculatedProperties];
    return self;
}

-(void)setSeries:(NSMutableArray *)series
{
    seriesArray = series;
    if(seriesArray.count>0) {
        xSeries = (Series *)([seriesArray objectAtIndex:0]);
        xMax = [xSeries.values count];
        width = (xMax-xMin)*25+2*xMargin;
    }
    [self setCalculatedProperties];
}

-(void)setRect:(CGRect)prect
{
    rect = prect;
    width = rect.size.width;
    height = rect.size.height - [Legend legendHeight];
    legend.yOrigin = height;
    legend.width = width;
    [self setCalculatedProperties];
}

-(void)setCalculatedProperties
{
    xSpacing = (width-2*xMargin)/(xMax-xMin);
    ySpacing = (height-2*yMargin)/(yMax-yMin);
    xOrigin = xMargin + abs(xSpacing*xMin);
    yOrigin = yMargin + ySpacing*yMax;
    seriesCount = seriesArray.count > 0 ? seriesArray.count-1 : 0;
    barWidth = xSpacing / (seriesCount+1);
}

-(void)draw
{
    [self fillChart];
    
    drawTextArray = [[NSMutableArray alloc] init];
    if(seriesArray.count>1)
    {
        for (int i=1; i < seriesArray.count; i++) {
            [self drawSeries:seriesArray[i]];
        }
    }
    
    [self drawXAxis];
    [self drawYAxis];
    [self drawText];
    [legend draw:seriesArray];
}
	
-(void) fillChart
{
    [backColor setFill];
    UIBezierPath *chartArea = [UIBezierPath bezierPathWithRect:rect];
    [chartArea fill];
    NSLog(@"Chart area filled");
}

-(void) drawXAxis
{
    [foreColor setStroke];
    UIBezierPath *xAxis = [UIBezierPath bezierPath];
    xAxis.lineWidth = axisWidth;
    [xAxis moveToPoint:CGPointMake(xOrigin + xMin*xSpacing, yOrigin)];
    [xAxis addLineToPoint:CGPointMake(width-xMargin, yOrigin)];
    [xAxis stroke];
    NSLog(@"xAxis drawn");
    
    for(int i = 1; i<xMax; i++)
    {
        float scalePointX = xOrigin + i*xSpacing;
        UIBezierPath *scale = [UIBezierPath bezierPath];
        scale.lineWidth = scaleWidth;
        [scale moveToPoint:CGPointMake(scalePointX,yOrigin)];
        
        if(i%xScaleRatio==0)
        {
            NSNumber *year = (NSNumber *)[xSeries.values objectAtIndex:i];
            NSString *scaleText = [NSString stringWithFormat:@"%i", [year intValue]];
            CGRect scaleRect = [scaleText boundingRectWithSize:CGSizeMake(100, 100) options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
            [scaleText drawAtPoint:CGPointMake(scalePointX-scaleRect.size.width/2, yOrigin + majorScaleHeight + 3) withAttributes:nil];
            [scale addLineToPoint:CGPointMake(scalePointX, yOrigin+majorScaleHeight)];
            [scale stroke];
        }
        else
        {
            [scale addLineToPoint:CGPointMake(scalePointX, yOrigin+minorScaleHeight)];
            [scale stroke];
        }
    }
    NSLog(@"Scales in xAxis drawn");
}

-(void) drawYAxis
{
    [foreColor setStroke];
    UIBezierPath *yAxis = [UIBezierPath bezierPath];
    yAxis.lineWidth = axisWidth;
    [yAxis moveToPoint:CGPointMake(xOrigin, yMargin)];
    [yAxis addLineToPoint:CGPointMake(xOrigin, height-yMargin)];
    [yAxis stroke];
    NSLog(@"yAxis drawn");
    
    for (int j=yMax; j >= yMin; j--) {
        if(j==0) continue;
        float scalePointY = yMargin + (yMax-j)*ySpacing;
        UIBezierPath *scale = [UIBezierPath bezierPath];
        scale.lineWidth = scaleWidth;
        [scale moveToPoint:CGPointMake(xOrigin,scalePointY)];
        
        if(j%yScaleRatio==0)
        {
            NSString *scaleText = [NSString stringWithFormat:@"%i", j];
            CGRect scaleRect = [scaleText boundingRectWithSize:CGSizeMake(100, 100) options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
            [scaleText drawAtPoint:CGPointMake(xOrigin-majorScaleHeight-scaleRect.size.width-3, scalePointY - scaleRect.size.height/2) withAttributes:nil];
            [scale addLineToPoint:CGPointMake(xOrigin-majorScaleHeight, scalePointY)];
            [scale stroke];
        }
        else
        {
            [scale addLineToPoint:CGPointMake(xOrigin-minorScaleHeight, scalePointY)];
            [scale stroke];
        }
    }
    
    NSLog(@"Scales in yAxis drawn");
}

-(void)drawSeries:(Series *)series
{
    if(series.isLine)
        [self drawLineSeries:series];
    else
        [self drawBarSeries:series];
}

-(void) drawBarSeries:(Series *)series
{
    [series.color setFill];
    for(int i=0; i < xMax; i++)
    {
        double seriesValue = [(NSNumber *)[series.values objectAtIndex:i] doubleValue];
        NSString *seriesValueText = [NSString stringWithFormat:@"%.1f", seriesValue];
        CGRect seriesValueTextSize = [seriesValueText boundingRectWithSize:CGSizeMake(100,100) options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
        
        double graphValue = seriesValue;
        if(seriesValue>yMax)
            graphValue=yMax;
        if(seriesValue<yMin)
            graphValue=yMin;
        bool showSeriesValue = !(seriesValue < 0.05 && seriesValue > -0.05);
        if(showSeriesValue) {
            if(seriesValue>0 && seriesValue<1)
                graphValue = 1;
            if(seriesValue<0 && seriesValue>-1)
                graphValue = -1;
        }
        CGPoint location = CGPointMake(xOrigin+i*xSpacing + (series.position-1)*barWidth, yOrigin - graphValue*ySpacing);
        CGSize size = CGSizeMake(barWidth, graphValue*ySpacing-axisWidth/2);
        CGPoint textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y - seriesValueTextSize.size.height-3);
        if(graphValue<0) {
            location =CGPointMake(xOrigin+i*xSpacing+(series.position-1)*barWidth, yOrigin + axisWidth/2);
            size = CGSizeMake(barWidth, abs(graphValue)*ySpacing);
            textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y + size.height+3);
        }
        
        if(series.showValue && showSeriesValue)
        {
            [drawTextArray addObject:[[DrawText alloc] init:seriesValueText location:textLocation]];
        }
        
        if(seriesValue!=0)
        {
            UIBezierPath *barPath = [UIBezierPath bezierPathWithRect:CGRectMake(location.x, location.y, size.width, size.height)];
            [barPath fill];
        }
    }
    NSLog(@"Bar series - %@ drawn", series.name);
}

// obsolete for the time being
-(void) drawLineSeries:(Series *)series
{
    UIBezierPath *lineGraph = [UIBezierPath bezierPath];
    lineGraph.lineWidth = axisWidth;
    double seriesValue = [(NSNumber *)[series.values objectAtIndex:0] doubleValue];
    NSString *seriesValueText = [NSString stringWithFormat:@"%.1f", seriesValue];
    CGRect seriesValueTextSize = [seriesValueText boundingRectWithSize:CGSizeMake(100,100) options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
    if(seriesValue>20)
        seriesValue=20;
    if(seriesValue<-20)
        seriesValue=-20;
    CGPoint location = CGPointMake(xOrigin+barWidth*(series.position-1)+barWidth/2, yOrigin-seriesValue*ySpacing);
    CGPoint textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y - seriesValueTextSize.size.height-3);
    if(seriesValue<0)
        textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y+3);
    if(series.showValue) {
        DrawText *dt = [[DrawText alloc] init:seriesValueText location:textLocation];	
        [drawTextArray addObject:dt];
    }
    
    [lineGraph moveToPoint:location];
    for(int i=1; i < xMax; i++)
    {
        seriesValue = [(NSNumber *)[series.values objectAtIndex:i] doubleValue];
        seriesValueText = [NSString stringWithFormat:@"%.2f", seriesValue];
        seriesValueTextSize = [seriesValueText boundingRectWithSize:CGSizeMake(100,100) options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
        if(seriesValue>20)
            seriesValue=20;
        if(seriesValue<-20)
            seriesValue=-20;
        location = CGPointMake(xOrigin+i*xSpacing+barWidth*(series.position-1)+barWidth/2, yOrigin-seriesValue*ySpacing);
        [lineGraph addLineToPoint:location];
        textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y - seriesValueTextSize.size.height-3);
        if(seriesValue<0)
            textLocation = CGPointMake(location.x + (barWidth-seriesValueTextSize.size.width)/2, location.y + 3);
        if(series.showValue)
            [drawTextArray addObject:[[DrawText alloc] init:seriesValueText location:textLocation]];
    }
    [series.color setStroke];
    [lineGraph stroke];
    NSLog(@"Line series - %@ drawn", series.name);
}

-(void)drawText
{
    for (DrawText *dt in drawTextArray) {
        [dt.text drawAtPoint:dt.location withAttributes:nil];
    }
    NSLog(@"Text drawn");
}

@end
