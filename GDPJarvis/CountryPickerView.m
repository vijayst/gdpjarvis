//
//  CountryPickerView.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 24/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "CountryPickerView.h"
#import "Country.h"

@implementation CountryPickerView

@synthesize componentWidth;
@synthesize controller;

-(id)initWithController:(ViewController *)pController
{
    self = [super init];
    if(self) {
        self.controller = pController;
        self.dataSource = self;
        self.delegate = self;
        self.backgroundColor = [UIColor colorWithRed:203.0/256 green:226.0/256 blue:256.0/256 alpha:1];
        self.componentWidth = 300;
    }
    return self;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return componentWidth;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0F;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    Country *country = [self.controller.countries objectAtIndex: row];
    return country.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    Country *country = [self.controller.countries objectAtIndex:row];
    if(country!=nil) {
        [self.controller changeCountry:country.shortName];
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.controller.countries count];
}


@end
