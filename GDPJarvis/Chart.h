//
//  Chart.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Series.h"
#import "Legend.h"

@interface Chart : NSObject
{
    NSMutableArray *drawTextArray;
    CGRect rect;
    NSMutableArray *seriesArray;
}
@property int xMin;
@property int xMax;
@property int yMin;
@property int yMax;
@property int width;
@property int height;
@property int xMargin;
@property int yMargin;
@property int xSpacing;
@property int ySpacing;
@property UIColor *foreColor;
@property UIColor *backColor;
@property int axisWidth;
@property int scaleWidth;
@property int majorScaleHeight;
@property int minorScaleHeight;
@property int xScaleRatio;
@property int yScaleRatio;
@property int xOrigin;
@property int yOrigin;
@property int seriesCount;
@property float barWidth;
@property Series *xSeries;
@property Legend *legend;

-(void)setRect:(CGRect)rect;
-(void)setSeries:(NSMutableArray *)array;
-(void)draw;
@end
