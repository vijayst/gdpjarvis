//
//  ViewController.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 25/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "ViewController.h"
#import "Region.h"
#import "Country.h"
#import "ChartView.h"
#import "RegionPickerView.h"
#import "CountryPickerView.h"

@interface ViewController ()
-(void)addControls;
-(UIButton *)addButton:(NSString *)buttonText buttonX:(float)buttonX buttonY:(float)buttonY action:(SEL)action;
-(NSArray *)getGdpText;
@end

@implementation ViewController
@synthesize regionLabel;
@synthesize regionPicker;

@synthesize countryLabel;
@synthesize countryPicker;

@synthesize scrollView;
@synthesize chartView;
@synthesize infoButton;
@synthesize explButton;
@synthesize analButton;

@synthesize regions;
@synthesize countries;
@synthesize gdpArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    gdpHelper = [[GdpDbHelper alloc] init];
    regions = [gdpHelper getRegions];
    Region *region = [regions objectAtIndex:0];
    countries = [gdpHelper getCountries:region.id];
    Country *country = [countries objectAtIndex:0];
    gdpArray = [gdpHelper getGdp:country.shortName];
    
    [self addControls];
    
    ttsHelper = [[TTSHelper alloc] init];
    gdpInterpreter = [[GdpInterpreter alloc] init];
    audioHelper = [[AudioHelper alloc] init];
    
    [countryPicker selectRow:3 inComponent:0 animated:NO];
    [self changeCountry:@"US"];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    float width = self.view.bounds.size.width;
    
    float countryPickerX = countryLabel.frame.origin.x + countryLabelWidth;
    countryPicker.frame = CGRectMake(countryPickerX, marginHeight, width-countryPickerX-marginWidth, uiPickerHeight);
    // Avoid circular dependency by having UIPickerViews!
    CountryPickerView *countryPickerCasted = (CountryPickerView *)countryPicker;
    countryPickerCasted.componentWidth = width-countryPickerX-marginWidth;
    [countryPicker reloadAllComponents];
    
    scrollView.frame = CGRectMake(marginWidth, uiPickerHeight+2*marginHeight, width-2*marginWidth, contentHeight);
    
    float buttonY = scrollView.frame.origin.y + contentHeight + marginHeight + (buttonGroupHeight-buttonHeight)/2;
    float buttonX = (width-buttonGroupWidth)/2;
    infoButton.frame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
    buttonX += buttonWidth + buttonSpacing;
    explButton.frame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
    buttonX += buttonWidth + buttonSpacing;
    analButton.frame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
}


-(void)addControls
{
    marginWidth = 30.0f;
    marginHeight=20.0f;
    
    uiPickerHeight = 180.0f;
    regionLabelWidth = 70.0f;
    countryLabelWidth = 100.0f;
    regionPickerWidth = 200.0f;
    
    buttonGroupHeight = 40.0f;
    buttonWidth = 180.0f;
    buttonHeight = 30.0f;
    buttonSpacing = 20.0f;
    
    contentHeight = 768.0f - 4*marginHeight - uiPickerHeight-buttonGroupHeight;
    
    float labelY = marginHeight + (uiPickerHeight-buttonHeight)/2;
    regionLabel = [[UILabel alloc] initWithFrame:CGRectMake(marginWidth, labelY, regionLabelWidth, buttonHeight)];
    regionLabel.text = @"Region";
    regionLabel.font = [UIFont systemFontOfSize:20.0];
    [self.view addSubview:regionLabel];
    
    regionPicker = [[RegionPickerView alloc] initWithController:self];
    regionPicker.frame = CGRectMake(regionLabel.frame.origin.x+regionLabelWidth, marginHeight, regionPickerWidth, uiPickerHeight);
    [self.view addSubview:regionPicker];
    NSLog(@"region picker height is %f", regionPicker.frame.size.height);
    
    countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(regionPicker.frame.origin.x+regionPickerWidth+marginWidth, labelY, countryLabelWidth, buttonHeight)];
    countryLabel.text = @"Country";
    countryLabel.font = [UIFont systemFontOfSize:20.0];
    [self.view addSubview:countryLabel];
    
    float width = self.view.bounds.size.width;
    countryPicker = [[CountryPickerView alloc] initWithController:self];
    float countryPickerX = countryLabel.frame.origin.x + countryLabelWidth;
    countryPicker.frame = CGRectMake(countryPickerX, marginHeight, width-countryPickerX-marginWidth, uiPickerHeight);
    [self.view addSubview:countryPicker];
    
    contentWidth = [gdpArray count]*100;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(marginWidth, uiPickerHeight+2*marginHeight, width-2*marginWidth, contentHeight)];
    chartView = [[ChartView alloc] initWithFrameAndData:CGRectMake(0,0,contentWidth,contentHeight) data:gdpArray];
    [scrollView addSubview:chartView];
    scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    scrollView.pagingEnabled = true;
    [self.view addSubview:scrollView];
    
    float buttonY = scrollView.frame.origin.y + contentHeight + marginHeight + (buttonGroupHeight-buttonHeight)/2;
    int totalNumberOfButtons = 3;
    buttonGroupWidth = buttonWidth * totalNumberOfButtons + buttonSpacing * (totalNumberOfButtons-1);
    
    float buttonX = (width-buttonGroupWidth)/2;
    infoButton = [self addButton:@"Explain GDP" buttonX:buttonX buttonY:buttonY action:@selector(explainGdp:)];
    
    buttonX += buttonWidth + buttonSpacing;
    explButton = [self addButton:@"Explain Chart" buttonX:buttonX buttonY:buttonY action:@selector(explainChart:)];
    
    buttonX += buttonWidth + buttonSpacing;
    analButton = [self addButton:@"Analyze" buttonX:buttonX buttonY:buttonY action:@selector(analyze:)];
}

-(UIButton *)addButton:(NSString *)buttonText  buttonX:(float)buttonX buttonY:(float)buttonY action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
    button.titleLabel.font = [UIFont systemFontOfSize:20.0];
    [button setTitle:buttonText forState:UIControlStateNormal];
    [button setTitle:@"Cancel" forState:UIControlStateSelected|UIControlStateHighlighted];
    [button setTitle:@"Cancel" forState:UIControlStateHighlighted];
    [button setTitle:@"Cancel" forState:UIControlStateSelected];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    return button;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)changeRegion:(int)regionId
{
    // play the sound
    [audioHelper playButtonSound];
    
    // Refresh the country picker
    countries = [gdpHelper getCountries:regionId];
    [countryPicker reloadAllComponents];
    
    // Refresh the chart with default country for region.
    Country *country = [countries firstObject];
    [countryPicker selectRow:0 inComponent:0 animated:false];
    [self changeCountry:country.shortName];
}

-(void)changeCountry:(NSString *)country
{
    //play the sound
    [audioHelper playButtonSound];
    
    gdpArray = [gdpHelper getGdp:country];
    contentWidth = [gdpArray count]*100;
    chartView.frame = CGRectMake(0,0,contentWidth, contentHeight);
    scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    [chartView setData:gdpArray];
    [chartView setNeedsDisplay];
}


-(void)getGdpForCountry:(NSString *)country
{
    gdpArray = [gdpHelper getGdp:country];
    contentWidth = [gdpArray count]*100;
    chartView.frame = CGRectMake(0,0,contentWidth, contentHeight);
    scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    [chartView setData:gdpArray];
    [chartView setNeedsDisplay];
}

-(void)explainGdp:(UIButton *)sender
{
    NSLog(@"Button state - %d", sender.state);
    if(sender.selected==NO) {
        [ttsHelper speakText:[self getGdpText] button:infoButton];
    } else {
        [ttsHelper cancelSpeech];
    }
}

-(void)explainChart:(UIButton *)sender
{
    NSLog(@"Button state - %d", sender.state);
    if(sender.selected==NO) {
        [ttsHelper speakText:[self getGdpChartText] button:explButton];
    } else {
        [ttsHelper cancelSpeech];
    }
}

-(void)analyze:(UIButton *)sender
{
    NSLog(@"Button state - %d", sender.state);
    NSArray *analyzeArray = [gdpInterpreter getGdpInterpretation:gdpArray];
    if(sender.selected==NO) {
        [ttsHelper speakText:analyzeArray button:analButton];
    } else {
        [ttsHelper cancelSpeech];
    }
}

-(NSArray *)getGdpText
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:@"GDP stands for Gross Domestic Product."];
    [array addObject:@"GDP is the sum of all goods and services produced in a country."];
    [array addObject:@"GDP Growth indicates how healthy an economy is."];
    [array addObject:@"Nominal GDP Growth is adjusted for inflation and currency fluctuations to get the Real GDP Growth."];
    return array;
}

-(NSArray *)getGdpChartText
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:@"The X axis shows the years for which GDP is measured."];
    [array addObject:@"The Y axis shows the percentage values of the various GDP factors."];
    [array addObject:@"Nominal GDP Growth Rate is shown by the green bar."];
    [array addObject:@"Inflation Rate is shown by the red bar."];
    [array addObject:@"GDP Deflator is shown by the orange bar."];
    [array addObject:@"Real GDP Growth Rate is shown by the blue bar"];
    return array;
}

@end
	