//
//  EntityBase.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 27/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EntityBase : NSObject
@property int id;
@property NSString *name;
@end
