//
//  Country.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 27/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntityBase.h"

@interface Country : EntityBase
@property int region;
@property NSString *shortName;
@end
