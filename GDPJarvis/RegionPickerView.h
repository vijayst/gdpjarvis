//
//  RegionPickerView.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 24/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface RegionPickerView : UIPickerView<UIPickerViewDelegate, UIPickerViewDataSource>
{

}

-(id)initWithController:(ViewController *)controller;

@property (strong, nonatomic) ViewController *controller;

@end
