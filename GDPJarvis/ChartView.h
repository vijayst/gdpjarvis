//
//  ChartView.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 06/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chart.h"

@interface ChartView : UIView
{
    NSMutableArray *gdpArray;
    Chart *chart;
}

-(id)initWithFrameAndData:(CGRect)frame data:(NSMutableArray *)data;
-(void)setData:(NSMutableArray *)data;
@end
