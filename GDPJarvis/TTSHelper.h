//
//  TTSHelper.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 18/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVSpeechSynthesis.h>

@interface TTSHelper : NSObject<AVSpeechSynthesizerDelegate>
{
    AVSpeechSynthesisVoice *voice;
    AVSpeechSynthesizer *speech;
    UIButton *currentButton;
    UIButton *nextCurrentButton;
    AVSpeechUtterance *lastUtterance;
    NSMutableArray *utterances;
    int position;
}
-(void)speakText:(NSArray *)textArray button:(UIButton *)button;
-(void)cancelSpeech;
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance;
@end
