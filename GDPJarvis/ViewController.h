//
//  ViewController.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 25/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVSpeechSynthesis.h>
#import "GdpDbHelper.h"
#import "TTSHelper.h"
#import "ChartView.h"
#import "GdpInterpreter.h"
#import "AudioHelper.h"

@interface ViewController : UIViewController
{
    GdpDbHelper *gdpHelper;
    TTSHelper *ttsHelper;
    GdpInterpreter *gdpInterpreter;
    AudioHelper *audioHelper;
    
    float contentWidth;
    float contentHeight;
    
    float marginWidth; // width spacing between controls and frame edges
    float marginHeight; // height spacing between controls and frame edges
    
    float uiPickerHeight; // 162, 180, 216;
    float regionLabelWidth;
    float countryLabelWidth;
    float regionPickerWidth;
    
    float buttonGroupWidth;
    float buttonGroupHeight;
    float buttonWidth;
    float buttonHeight;
    float buttonSpacing;
}
@property (strong, nonatomic) UILabel *regionLabel;
@property (strong, nonatomic) UIPickerView *regionPicker;

@property (strong, nonatomic) UILabel *countryLabel;
@property (strong, nonatomic) UIPickerView *countryPicker;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) ChartView *chartView;

@property (nonatomic, retain) UIButton *infoButton;
@property (nonatomic, retain) UIButton *explButton;
@property (nonatomic, retain) UIButton *analButton;

@property (strong, nonatomic) NSMutableArray *regions;
@property (strong, nonatomic) NSMutableArray *countries;
@property (strong, nonatomic) NSMutableArray *gdpArray;

-(void)changeRegion:(int)regionId;
-(void)changeCountry:(NSString *)country;

-(void)explainGdp:(UIButton *)sender;
-(void)explainChart:(UIButton *)sender;
-(void)analyze:(UIButton *)sender;
@end
