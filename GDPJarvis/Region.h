//
//  Region.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 26/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntityBase.h"

@interface Region : EntityBase
@end