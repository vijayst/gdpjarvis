//
//  main.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 25/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
