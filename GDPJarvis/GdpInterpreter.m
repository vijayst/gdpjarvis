//
//  GdpInterpreter.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 28/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "GdpInterpreter.h"
#import "Gdp.h"

@interface GdpInterpreter ()
-(void)getPeriod;
-(void)getNominalGrowth;
-(void)getRealGrowth;
-(void)getRecessionYears;
-(void)updateArray;
-(NSString *)getNumberInWords:(double)number;
@end

@implementation GdpInterpreter

-(id)init
{
    self = [super init];
    if(self!=nil) {
        array = [[NSMutableArray alloc] init];
        recessionYears = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSArray *)getGdpInterpretation:(NSArray *)pGdpArray
{
    gdpArray = pGdpArray;
    [array removeAllObjects];
    
//    for (Gdp *gdp in gdpArray) {
//        if(gdp == [gdpArray firstObject]) {
//            [self resetSeries:gdp];
//        } else {
//            bool curGrowth = gdp.growth >= 0;
//            if(curGrowth!=growth) {
//                [self updateArray];
//                [self resetSeries:gdp];
//            } else {
//                toYear = gdp.year;
//                sum += gdp.growth;
//            }
//        }
//    }
    [self getPeriod];
    [self getNominalGrowth];
    [self getRealGrowth];
    [self getRecessionYears];
    [self updateArray];
    return array;
}

-(void)getPeriod
{
    // Get the first year where both Gdp & GdpGrowth are non-zero
    // Get the last year where both Gdp & GdpGrowth are non-zero
    fromYearIndex=-1;
    toYearIndex=-1;
    for(int i=0; i<[gdpArray count]; i++) {
        Gdp *gdp = [gdpArray objectAtIndex:i];
        bool condition = gdp.nominalGdp > 0 && gdp.growth != 0;
        if(condition && fromYearIndex==-1) {
            fromYearIndex = i;
            fromYear = gdp.year;
        }
        if(condition) {
            toYearIndex = i;
            toYear = gdp.year;
        }
    }
}

-(void)getNominalGrowth
{
    Gdp* from = [gdpArray objectAtIndex:fromYearIndex];
    fromGdp = from.nominalGdp;
    Gdp *to = [gdpArray objectAtIndex:toYearIndex];
    toGdp = to.nominalGdp;
    int n = toYearIndex - fromYearIndex + 1;
    double p1 = toGdp/fromGdp;
    double p2 = 1.0/n;
    avgNominalGrowth = (pow(p1,p2)-1)*100;
}

-(void)getRealGrowth
{
    double p1 = 1.0;
    for(int i=fromYearIndex; i<=toYearIndex; i++) {
        Gdp *gdp = [gdpArray objectAtIndex:i];
        p1 = p1 * (1+gdp.growth/100);
    }
    int n = toYearIndex - fromYearIndex + 1;
    double p2 = 1.0/n;
    avgRealGrowth = (pow(p1,p2)-1)*100;
}

-(void)getRecessionYears
{
    [recessionYears removeAllObjects];
    for(int i=fromYearIndex; i<=toYearIndex; i++) {
        Gdp *gdp = [gdpArray objectAtIndex:i];
        if(gdp.growth<0)
            [recessionYears addObject:[NSNumber numberWithInt:gdp.year]];
    }
}

-(NSString *)getNumberInWords:(double)number
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.usesSignificantDigits = TRUE;
    formatter.maximumSignificantDigits = 3;
    [formatter setNumberStyle:NSNumberFormatterScientificStyle];
    NSString *tmpString = [formatter stringFromNumber:[NSNumber numberWithDouble:number]];
    NSNumber *tmpNumber = [formatter numberFromString:tmpString];
    [formatter setNumberStyle:NSNumberFormatterSpellOutStyle];
    return [formatter stringFromNumber:tmpNumber];
}

//
//-(void)resetSeries:(Gdp *)gdp
//{
//    fromYear = gdp.year;
//    toYear = gdp.year;
//    growth = gdp.growth >= 0;
//    sum = gdp.growth;
//}

-(void)updateArray
{
//    NSString *growString = growth ? @"Growth" : @"Recession and Decline";
//    if(fromYear==toYear)
//        [array addObject:[NSString stringWithFormat:@"%@ by %1.1f during %d", growString, sum, fromYear]];
//    else {
//        float average = sum / (toYear - fromYear + 1);
//        [array addObject:[NSString stringWithFormat:@"%@ by %1.1f between %d and %d", growString, average, fromYear, toYear]];
//    }

    NSString *fromGdpText = [self getNumberInWords:fromGdp];
    NSString *toGdpText = [self getNumberInWords:toGdp];
    [array addObject:[NSString stringWithFormat:@"Nominal GDP Growth averaged %1.1f%% between %d to %d.", avgNominalGrowth, fromYear, toYear]];
    [array addObject:[NSString stringWithFormat:@"Real GDP Growth averaged %1.1f%% between %d to %d.", avgRealGrowth, fromYear, toYear]];
    [array addObject:[NSString stringWithFormat:@"GDP grew from %@ to %@ between the year %d to the year %d.", fromGdpText, toGdpText, fromYear, toYear]];
    
    if([recessionYears count] == 0) {
        [array addObject:@"There were no years of negative GDP Growth"];
    } else {
        NSString *yearString = [recessionYears componentsJoinedByString:@", "];
        [array addObject:[NSString stringWithFormat:@"Negative GDP Growth was witnessed in the years: %@", yearString]];
    }
}

@end
