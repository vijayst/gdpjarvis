//
//  TTSHelper.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 18/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "TTSHelper.h"

@interface TTSHelper()
-(void)speakInternal;
@end

@implementation TTSHelper
-(id)init
{
    self = [super init];
    if(self!=nil) {
        voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-GB"];
        utterances = [[NSMutableArray alloc] init];
    }
    return self;
}


-(void)speakText:(NSArray *)textArray button:(UIButton *)button
{
    nextCurrentButton = button;
    if(speech.speaking) {
        [self cancelSpeech];
        
        if(nextCurrentButton!=nil) {
            currentButton = nextCurrentButton;
            nextCurrentButton = nil;
        }
    }
    else {
        currentButton = button;
        nextCurrentButton = nil;
    }
    
    currentButton.selected = YES;
    [utterances removeAllObjects];
    for (NSString *text in textArray) {
        AVSpeechUtterance *welcome = [[AVSpeechUtterance alloc] initWithString:text];
        welcome.rate = 0.20;
        welcome.voice = voice;
        welcome.pitchMultiplier = 1.2;
        welcome.preUtteranceDelay = 0.25;
        [utterances addObject:welcome];
    }	
    lastUtterance = [utterances lastObject];
    position = 0;
    [self speakInternal];
}

-(void)speakInternal
{
    if([utterances count] > position) {
        speech = [[AVSpeechSynthesizer alloc] init];
        speech.delegate = self;
        [speech speakUtterance:[utterances objectAtIndex:position++]];
    }
}

-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    if(lastUtterance==utterance) {
        currentButton.selected = NO;
    }
    else {
        [self speakInternal];
    }
}


-(void)cancelSpeech
{
    [utterances removeAllObjects];
    currentButton.selected = NO;
    position = 0;
    BOOL stopped = [speech stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    NSLog(@"stopped - %d", stopped);
}

@end
