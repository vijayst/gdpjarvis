//
//  Series.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Series : NSObject
@property NSMutableArray *values;
@property int position;
@property NSString *name;
@property UIColor *color;
@property bool isLine;
@property bool showValue;
@end
