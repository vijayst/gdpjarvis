//
//  GdpDbHelper.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 25/09/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface GdpDbHelper: NSObject
{
    sqlite3 *database;
}
-(NSMutableArray*)getRegions;
-(NSMutableArray*)getCountries:(int)region;
-(NSMutableArray*)getGdp:(NSString *)country;
@end
