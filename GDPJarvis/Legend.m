//
//  ChartLegend.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "Legend.h"
#import "Series.h"
#import <UIKit/UIFont.h>

static int LegendHeight = 50;
static int LegendMargin = 50;

@implementation Legend
@synthesize xOrigin;
@synthesize yOrigin;
@synthesize width;
@synthesize height;
@synthesize backColor;
@synthesize cellWidth;
@synthesize cellHeight;
@synthesize cellSpacing;
@synthesize textSpacing;
@synthesize attributes;



+(int)legendHeight
{
    return LegendHeight;
}

-(id)init
{
    xOrigin = 0;
    yOrigin = 500;
    width = 500;
    height = LegendHeight;
    backColor = [UIColor lightGrayColor];
    cellWidth = 60;
    cellHeight = 20;
    cellSpacing = 20;
    textSpacing = 10;
    UIFont *font = [UIFont fontWithName:@"Palatino-Roman" size:16.0];
    attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    return self;
}

-(void)draw:(NSMutableArray *)seriesArray
{
    UIBezierPath *rect = [UIBezierPath bezierPathWithRect:CGRectMake(xOrigin, yOrigin, width, height)];
    [backColor setFill];	
    [rect fill];
    
    float fontHeight = [self getFontHeight];
    float yFill = yOrigin + (LegendHeight-cellHeight)/2;
    float yText = yOrigin + (LegendHeight-fontHeight)/2;
    
    CGPoint location = CGPointMake(xOrigin + LegendMargin, yText);
    for(int i=1; i<[seriesArray count]; i++)
    {
        Series *series = [seriesArray objectAtIndex:i];
        location.y = yFill;
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(location.x, location.y, cellWidth, cellHeight)];
        [series.color setFill];
        [path fill];
        location.x += cellWidth + textSpacing;
        location.y = yText;
        [series.name drawAtPoint:location withAttributes:attributes];
        location.x += [self getFontWidth:series.name] + cellSpacing;
    }
}

-(float)getFontHeight
{
    NSString *test = @"test";
    CGRect rect = [test boundingRectWithSize:CGSizeMake(100,100) options:NSStringDrawingUsesDeviceMetrics attributes:attributes context:nil];
    return rect.size.height;
}

-(float)getFontWidth:(NSString *)text
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(100,100) options:NSStringDrawingUsesDeviceMetrics attributes:attributes context:nil];
    return rect.size.width;
}

@end
