//
//  RegionPickerView.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 24/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "RegionPickerView.h"
#import "Region.h"


@implementation RegionPickerView

-(id)initWithController:(ViewController *)pController
{
    self = [super init];
    if(self) {
        self.controller = pController;
        self.dataSource = self;
        self.delegate = self;
        self.backgroundColor = [UIColor colorWithRed:203.0/256 green:226.0/256 blue:256.0/256 alpha:1];
    }
    return self;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 200.0F;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0F;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    Region *region = [self.controller.regions objectAtIndex: row];
    return region.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    Region *region = [self.controller.regions objectAtIndex:row];
    if(region!=nil) {
        [self.controller changeRegion:region.id];
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.controller.regions count];
}


@end
