//
//  DrawText.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 07/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "DrawText.h"

@implementation DrawText
@synthesize text;
@synthesize location;

-(DrawText *)init:(NSString *)ptext location:(CGPoint)plocation
{
    text = ptext;
    location = plocation;
    return self;
}
@end
