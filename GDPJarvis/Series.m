//
//  Series.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "Series.h"

@implementation Series
@synthesize values;
@synthesize position;
@synthesize name;
@synthesize color;
@end
