//
//  ChartLegend.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 05/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Legend : NSObject
+(int)legendHeight;
@property int xOrigin;
@property int yOrigin;
@property int width;
@property int height;
@property UIColor *backColor;
@property int cellWidth;
@property int cellHeight;
@property int cellSpacing;
@property int textSpacing;
@property NSDictionary *attributes;
-(void)draw:(NSMutableArray *)series;
@end
