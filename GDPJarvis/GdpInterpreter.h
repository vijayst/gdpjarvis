//
//  GdpInterpreter.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 28/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GdpInterpreter : NSObject
{
    int fromYear;
    int toYear;
    int fromYearIndex;
    int toYearIndex;
    double fromGdp;
    double toGdp;
    double avgNominalGrowth;
    double avgRealGrowth;
    NSMutableArray *recessionYears;
    
    NSArray *gdpArray;
    NSMutableArray *array;
}
-(NSArray *)getGdpInterpretation:(NSArray *)gdpArray;
@end
