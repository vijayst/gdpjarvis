//
//  AudioHelper.h
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface AudioHelper : NSObject<AVAudioPlayerDelegate>
-(void)playLoadSound;
-(void)playButtonSound;
@property (nonatomic, retain) AVAudioPlayer *player;
@property (nonatomic, retain) AVAudioPlayer *loadPlayer; // property for retaining
@end
