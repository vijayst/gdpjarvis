//
//  AudioHelper.m
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "AudioHelper.h"

@implementation AudioHelper
@synthesize player;
@synthesize loadPlayer;

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"button" ofType:@"mp3" inDirectory:@""];
        NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
        NSError *error;
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
        NSLog(@"Error in loading - %@", error.description);
        player.delegate = self;
    }
    return self;
}

-(void)playButtonSound
{
    [player play];
}

-(void)playLoadSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gameload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    NSError *error;
    loadPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
    NSLog(@"Error in loading - %@", error.description);
    loadPlayer.delegate = self;
    [loadPlayer play];
}

@end
